package Tests;

import domain.User;
import domain.UserType;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class UserTest  {
    User user = null;

    @Before
    public void setUp() {
        user = new User();
        user.setUsertype(UserType.REGULAR);
        user.setUsername("Karol");
        user.setEmail("karolloczek@gmail.com");
        user.setPassword("spica");
    }

    @Test
    public void testUserObject() {
        assertEquals(user.getUsername(), "Karol");
        assertEquals(user.getUsertype(), UserType.REGULAR);
        assertEquals(user.getPassword(), "spica");
        assertEquals(user.getEmail(),"karolloczek@gmail.com");
    }
}
