package web;

import domain.User;
import domain.UserType;
import repository.Repository;
import repository.UserRepository;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebServlet("/ShowUsers")
public class ShowUsers extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        session.setAttribute("currentUser", UserType.ADMIN);
        Repository repo = new UserRepository();

        showUsersTable(request, response, createTableOfUsers(repo));
    }


    private String createTableOfUsers(Repository repo) {
        StringBuilder table = new StringBuilder();
        repo.getAllUsers().forEach(user -> table
                .append("<tr><th>")
                .append(user.getUsername())
                .append("</th<th>")
                .append(user.getUsertype())
                .append("</th></tr><br/>"));
        return table.toString();

    }

/*
    private  String createTableOfUsers(Repository repo){
        StringBuilder table = new StringBuilder();

        for(User user : repo.getAllUsers()){
            table.append("<tr><th>")
                    .append(user.getUsername())
                    .append("</th><th>")
                    .append(user.getUsertype())
                    .append("</th></tr><br/>");
        }
        return table.toString();
    }
    */
    /*
    private String createTableOfUsers(Repository repo){
        String table = "";
        for(User user: repo.getAllUsers()) {
            table +=("<tr><th>"+user.getUsername()+"</th><th>"+user.getUsertype()+"</th></tr><br/>");
        }
        return  table;
    }
    */
    private void showUsersTable(HttpServletRequest request, HttpServletResponse response, String table) throws ServletException, IOException {
        request.setAttribute("message", table);
        request.getRequestDispatcher("/chmod.jsp").forward(request,response);
    }
}
