package domain;

public enum UserType {
    REGULAR, PREMIUM, ADMIN, ANONIM;
}
